#!/bin/bash

# install pre-requisites
brew install cmake git googletest

# build and test
./tools/ci/buildtest_cmake.sh