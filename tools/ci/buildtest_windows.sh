#!/bin/bash
# we consider MSYS2 with cmake, git, mingw-w64-ucrt-x86_64-gcc is installed and
# that path env. var. are updated with CC=gcc, CXX=g++,
# PATH=C:\msys64\mingw64\bin;C:\msys64\usr\bin;C:\msys64\ucrt64\bin; etc

# build and test
./tools/ci/buildtest_cmake.sh