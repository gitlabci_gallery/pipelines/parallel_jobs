#!/bin/bash
# use this script in a docker ubuntu container

# install pre-requisites
apt update -y
apt install -y build-essential clang cmake git libgtest-dev

# build and test
./tools/ci/buildtest_cmake.sh