# Parallel Jobs

This example shows how to define matrix of jobs which can run concurrently,
possibly on different runners (and Operating Systems) and which can share the
same execution script. This is helpfull to factorize gitlab-ci jobs definitions.

## Source code

The source code is a trivial _Hello World_ in C++, see `src/hello.cpp` and
`tests/tests.cpp` defining a test (gtest). The project must be built with CMake
and googletest is required.
```sh
cmake .
make
ctest
```

## Gitlab-CI configuration

The gitlab-ci configuration is written in the file
[.gitlab-ci.yml](.gitlab-ci.yml). See the results of the pipeline
[here](https://gitlab.inria.fr/gitlabci_gallery/pipelines/parallel_jobs/-/pipelines).

We have set two gitlab-ci [runners](https://docs.gitlab.com/runner/install/),
one Linux with docker executor and one Macosx with shell executor.

The matrix of jobs is defined thanks to the keywords `parallel` and `matrix`,
followed by some variables to be defined with possible values. These variables
are the axes of the matrix (tensor if more than 2 variables in parallel).
For example the following defines a parallel matrix of 4 jobs
```
  parallel:
    matrix:
      - VM_TAG: ['linux', 'macosx']
        CXX: ['g++', 'clang++']
```

| VM_TAG/CXX | linux | macosx |
|------------|-------|--------|
| __g++__  | linux/g++ | macosx/g++ |
| __clang++__ | linux/clang++ | macosx/clang++ |

During each of these jobs an environment variable is defined for each variable
containing the appropriate value, e.g. the matrix element (1,1) `VM_TAG=linux`
and `CXX=g++`.

One can also define non combinatory configuration, for instance the following
defines 3 jobs, one with `VM_TAG=linux` (GCC undefined), one with `CXX=g++`
(VM_TAG undefined) and one with `CXX=clang++`.
```
  parallel:
    matrix:
      - VM_TAG: 'linux'
      - CXX: ['g++', 'clang++']
```

Combinations can also be constrained, the following defines 3 jobs
```
  parallel:
    matrix:
      - VM_TAG: 'linux'
        CXX: ['g++', 'clang++']
      - VM_TAG: 'macosx'
        CXX: 'clang++'
```

| VM_TAG/CXX | linux | macosx |
|------------|-------|--------|
| __g++__  | linux/g++ |  |
| __clang++__ | linux/clang++ | macosx/clang++ |