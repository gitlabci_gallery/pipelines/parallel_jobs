#ifndef HELLO_HPP
#define HELLO_HPP

#include <iostream>

class HelloWorld
{
    public:
        void static PrintHelloWorld()
        {
            std::cout << "Hello World!\n";
        }

};

#endif // HELLO_HPP